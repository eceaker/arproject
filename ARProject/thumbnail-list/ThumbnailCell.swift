//
//  ThumbnailCell.swift
//  ARProject
//
//  Created by Merih Ece Aker on 21.09.2019.
//  Copyright © 2019 Merih Ece Aker. All rights reserved.
//

import UIKit

class ThumbnailCell: UICollectionViewCell {
    
    static let identifier = String.init(describing: ThumbnailCell.self)

    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
