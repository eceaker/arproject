//
//  ViewController.swift
//  ARProject
//
//  Created by Merih Ece Aker on 19.09.2019.
//  Copyright © 2019 Merih Ece Aker. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController {

    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var planeAnchor : ARPlaneAnchor? = nil
    
    var thumbnails = [UIImage?](repeating: nil, count: 10)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneView.delegate = self
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.backgroundView = nil
        collectionView.register(UINib(nibName: ThumbnailCell.identifier, bundle: nil), forCellWithReuseIdentifier: ThumbnailCell.identifier)
        
        thumbnails[0] = UIImage(named: "emma-dancing.jpg")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // init the scene
        self.initScene()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // pause the scene view's session
        sceneView.session.pause()
    }
    
    func createDancingModel(at position : SCNVector3) {
        guard let scene = SCNScene.assimpSceneNamed("art.scnassets/emma-samba.fbx", postProcessFlags:  AssimpKitPostProcessSteps.init(arrayLiteral: [.process_FlipUVs, .process_Triangulate])) else { return }
        
        let node = SCNNode()
        
        for child in scene.rootNode.childNodes {
            node.addChildNode(child)
        }
        
        node.position = position
        node.scale = SCNVector3(0.00001, 0.00001, 0.00001)
        
        sceneView.scene.rootNode.addChildNode(node)
    }
    
    func createModel(at position : SCNVector3) {
        let scene = SCNScene(named: "art.scnassets/emma-dance.DAE")!
        let node = SCNNode()
        
        for child in scene.rootNode.childNodes {
            node.addChildNode(child)
        }
        
        node.position = position
        node.scale = SCNVector3(0.000025, 0.000025, 0.000025)
        
        sceneView.scene.rootNode.addChildNode(node)
    }
    
    func initScene() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        
        sceneView.session.run(configuration)
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let location = touches.first!.location(in: sceneView)
        let results = sceneView.hitTest(location, types: .existingPlaneUsingExtent)
        
        if let result = results.first {
            let translation = result.worldTransform.translation
            let x = translation.x
            let y = translation.y
            let z = translation.z
            self.createModel(at: SCNVector3(x,y,z))
        }
    }
}

extension float4x4 {
    var translation: float3 {
        let translation = self.columns.3
        return float3(translation.x, translation.y, translation.z)
    }
}

extension ViewController : ARSCNViewDelegate {

    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        if self.planeAnchor == nil {
            guard let planeAnchor = anchor as? ARPlaneAnchor else { return nil}
            self.planeAnchor = planeAnchor
            sceneView.debugOptions = []
            return SCNNode()
        }
        else {
            return nil
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        let width = CGFloat(planeAnchor.extent.x)
        let height = CGFloat(planeAnchor.extent.z)/2
        let plane = SCNPlane(width: width, height: height)
        
        plane.materials.first?.diffuse.contents = UIColor(red: 90/255, green: 200/255, blue: 250/255, alpha: 0.2)
        let planeNode = SCNNode(geometry: plane)
        
        let x = CGFloat(planeAnchor.center.x)
        let y = CGFloat(planeAnchor.center.y)
        let z = CGFloat(planeAnchor.center.z)
        planeNode.position = SCNVector3(x,y,z)
        planeNode.eulerAngles.x = -.pi / 2
        
        node.addChildNode(planeNode)
    }
}

extension ViewController: UICollectionViewDelegate {
    
    
}

extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return thumbnails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ThumbnailCell.identifier, for: indexPath) as! ThumbnailCell
        
        cell.imageView.image = thumbnails[indexPath.row]
        
        return cell
    }
    
}
