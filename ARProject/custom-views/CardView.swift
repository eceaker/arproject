//
//  CardView.swift
//  MessageApp
//
//  Created by Merih Ece Aker on 1.09.2019.
//  Copyright © 2019 Merih Ece Aker. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CardView : UIView {
    
    @IBInspectable var isCircle: Bool = false {
        didSet{
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var isRounded: Bool = false {
        didSet{
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var radius: CGFloat = 0 {
        didSet{
            setNeedsLayout()
        }
    }
    
    @IBInspectable var topLeft: Bool = true{
        didSet{
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var topRight: Bool = true{
        didSet{
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var bottomLeft: Bool = true{
        didSet{
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var bottomRight: Bool = true{
        didSet{
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var border: Bool = true {
        didSet{
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet{
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var borderColor: UIColor = .clear {
        didSet{
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var shadow: Bool = false {
        didSet{
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var shadowColor: UIColor = .clear {
        didSet{
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 0.0 {
        didSet{
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0.0 {
        didSet{
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var shadowOffset: CGSize = .zero {
        didSet{
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var forceClipping: Bool = false {
        didSet{
            setNeedsDisplay()
        }
    }
    
    var _backgroundColor: UIColor = .white
    
    override var backgroundColor: UIColor? {
        get {
            return .clear
        }
        set {
            if newValue != .clear {
                _backgroundColor = newValue ?? .white
                setNeedsDisplay()
            }
        }
    }
    
    var highlightColor: UIColor = .white
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateCornerRadius()
    }
    
    var borderLayer: CALayer?
    var shadowLayer: CALayer?
    
    // https://stackoverflow.com/questions/4754392/uiview-with-rounded-corners-and-drop-shadow?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
    func updateCornerRadius() {
        
        var path: UIBezierPath? = nil
        
        if isCircle {
            let cornerRadius = isCircle ? frame.size.height / 2 : radius
            path = UIBezierPath(arcCenter: CGPoint(x: bounds.midX, y: bounds.midY), radius: cornerRadius, startAngle: 0, endAngle: .pi * 2, clockwise: true)
        } else if (isRounded) {
            let cornerRadius = min(frame.size.width, frame.size.height) / 2
            path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        } else {
            var corners = UIRectCorner()
            if topLeft{
                corners.insert(.topLeft)
            }
            if topRight{
                corners.insert(.topRight)
            }
            if bottomLeft{
                corners.insert(.bottomLeft)
            }
            if bottomRight{
                corners.insert(.bottomRight)
            }
            
            path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            // path = UIBezierPath(roundedRect: bounds, cornerRadius: radius)
        }
        
        // clipsToBounds = false
        layer.masksToBounds = false
        
        // update mask and save for future reference
        let mask = CAShapeLayer()
        mask.path = path?.cgPath
        
        if (forceClipping) {
            layer.mask = mask
            layer.backgroundColor = _backgroundColor.cgColor
        } else {
            layer.mask = nil
            layer.backgroundColor = nil
            
            if(shadow){
                mask.shadowColor = shadowColor.cgColor
                mask.shadowOffset = shadowOffset
                mask.shadowOpacity = shadowOpacity
                mask.shadowRadius = shadowRadius
            }
            mask.fillColor = _backgroundColor.cgColor
            layer.insertSublayer(mask, at: 0)
            // layer.mask = mask
            
            shadowLayer?.removeFromSuperlayer()
            shadowLayer = mask
        }
        
        // if we had previous border remove it, add new one, and save reference to new one
        borderLayer?.removeFromSuperlayer()
        // create border layer
        if (border && borderWidth > 0) {
            let frameLayer = CAShapeLayer()
            frameLayer.path = path?.cgPath
            frameLayer.lineWidth = borderWidth / UIScreen.main.scale
            frameLayer.strokeColor = borderColor.cgColor
            frameLayer.fillColor = nil
            frameLayer.rasterizationScale = UIScreen.main.scale
            frameLayer.shouldRasterize = true
            
            borderLayer = frameLayer
            layer.addSublayer(frameLayer)
        }
        
    }
    
}
